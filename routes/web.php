<?php

use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('layouts');
});

Route::post('get-makes', [OrderController::class, 'getModels']);
Route::post('get-model-years', [OrderController::class, 'getModelYears']);
Route::post('place-order', [OrderController::class, 'placeOrder']);

Route::get('/set-locale/{locale}', function (string $locale) {
    if (! in_array($locale, ['en', 'ro', 'ru'])) {
        abort(400);
    }
    session()->put('locale', $locale);
    App::setLocale($locale);

    return redirect('/');
});

