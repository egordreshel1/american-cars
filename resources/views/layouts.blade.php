<!doctype html>
<html class="no-js" lang="en">

<head>
    <!-- meta data -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!--font-family-->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <link href="https://fonts.googleapis.com/css?family=Rufina:400,700" rel="stylesheet">

    <!-- title of site -->
    <title>Andrei parts</title>

    <!-- For favicon png -->
    <link rel="shortcut icon" type="image/icon" href="logo/favicon.png" />

    <!--font-awesome.min.css-->
    <link rel="stylesheet" href="css/font-awesome.min.css">

    <!--linear icon css-->
    <link rel="stylesheet" href="css/linearicons.css">

    <!--flaticon.css-->
    <link rel="stylesheet" href="css/flaticon.css">

    <!--animate.css-->
    <link rel="stylesheet" href="/css/animate.css">

    <!--owl.carousel.css-->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">

    <!--bootstrap.min.css-->
    <link rel="stylesheet" href="css/bootstrap.min.css">

    <!-- bootsnav -->
    <link rel="stylesheet" href="css/bootsnav.css">

    <!--style.css-->
    <link rel="stylesheet" href="css/app.css">

    <!--responsive.css-->
    <link rel="stylesheet" href="css/responsive.css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
    your browser</a> to improve your experience and security.</p>
<![endif]-->
@if(session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
<!--welcome-hero start -->
<section id="home" class="welcome-hero">

    <!-- top-area Start -->
    <div class="top-area">
        <div class="header-area">
            <!-- Start Navigation -->
            <nav class="navbar navbar-default bootsnav  navbar-sticky navbar-scrollspy" data-minus-value-desktop="70"
                 data-minus-value-mobile="55" data-speed="1000">

                <div class="container">

                    <!-- Start Header Navigation -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu">
                            <i class="fa fa-bars"></i>
                        </button>
                        <a class="navbar-brand" href="/">{{__('messages.welcome_title')}}<span></span></a>

                    </div><!--/.navbar-header-->
                    <!-- End Header Navigation -->

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse menu-ui-design" id="navbar-menu">
                        <ul class="nav navbar-nav navbar-right" data-in="fadeInDown" data-out="fadeOutUp">

                            <li class="scroll"><a href="#home">{{ __('messages.home') }}</a></li>
                            <li class="scroll"><a href="#contact">{{ __('messages.contact') }}</a></li>
                            <li><span style="padding-right: 205px;"></span></li>
                            <li class="">
                                <a style="color: {{ app()->currentLocale() === 'en' ? '#4e4ffa' : '' }}"
                                   id="language_en"
                                   href="{{ url('/set-locale/en') }}">EN</a>
                            </li>
                            <li class=" {{ app()->currentLocale() === 'ro' ? '#4e4ffa' : '' }}">
                                <a style="color: {{ app()->currentLocale() === 'ro' ? '#4e4ffa' : '' }}"
                                   id="language_ro"
                                   href="{{ url('/set-locale/ro') }}">RO</a>
                            </li>
                            <li class="  {{ app()->currentLocale() === 'ru' ? '#4e4ffa' : '' }}">
                                <a style="color: {{ app()->currentLocale() === 'ru' ? '#4e4ffa' : '' }}"
                                   id="language_ru"
                                   href="{{ url('/set-locale/ru') }}">RU</a>
                            </li>

                        </ul><!--/.nav -->
                    </div><!-- /.navbar-collapse -->
                </div><!--/.container-->
            </nav><!--/nav-->
            <!-- End Navigation -->
        </div><!--/.header-area-->
        <div class="clearfix"></div>

    </div><!-- /.top-area-->
    <!-- top-area End -->

    <div class="container">
        <div class="welcome-hero-txt">
            <h2>{{ __('messages.welcome-title') }}</h2>
            <p>
                We deliver parts very fast.
            </p>
            <button class="welcome-btn" id="contact-us">{{ __('messages.contact-us') }}</button>
        </div>
    </div>


</section><!--/.welcome-hero-->
<section id="new-cars" class="new-cars">
    <div class="container">
        <div class="section-header">
{{--            <p>checkout <span>the</span> latest cars</p>--}}
            <h2 style="text-transform: none;">{{ __('messages.parts-information') }}</h2>
        </div><!--/.section-header-->
        <div class="new-cars-content">
            <div class="owl-carousel owl-theme" id="new-cars-carousel">
                @foreach(\App\Models\Image::query()->orderBy('order')->get() as $image)
                    {{--                <div class="new-cars-item">--}}
                    {{--                    <div class="single-new-cars-item">--}}
                    <div class="new-cars-img">
                        <img src="{{ 'storage/' . $image->filename }}" alt="img" />
                    </div><!--/.new-cars-img-->
                    {{--                    </div><!--/.single-new-cars-item-->--}}
                    {{--                </div><!--/.new-cars-item-->--}}
                @endforeach
                {{--            </div><!--/#new-cars-carousel-->--}}
            </div><!--/.new-cars-content-->
        </div><!--/.container-->
    </div><!--/.container-->

</section><!--/.new-cars-->
<!--new-cars end -->
<section id="contact-form">
    <div class="container" style="width: 1330px;">
        <form method="POST" action="{{ url('place-order') }}" name="place-order">
            @csrf
            @method('POST')
            <div class="row">
                <div class="col-md-12">
                    <div class="model-search-content">
                        <div class="row">
                            <div class="col-md-offset-1 col-md-2 col-sm-12">
                                <div class="single-model-search">
                                    <h2>{{ __('messages.vehicle') }}</h2>
                                    <div class="model-select-icon">
                                        <select class="form-control" name="vehicle_id" id="make" required>
                                            <option selected disabled hidden></option>
                                            @foreach(\App\Models\Vehicle::all() as $vehicle)
                                                <option value="{{ $vehicle->id }}">{{ $vehicle->name }}</option>
                                                <!-- /.option-->
                                            @endforeach
                                            <option value="{{ null }}">{{ __('messages.other') }}</option>

                                        </select><!-- /.select-->
                                    </div><!-- /.model-select-icon -->
                                </div>
                                <div class="single-model-search">
                                    <h2>{{ __('VIN') }}</h2>
                                    <div class="model-select-without-icon">
                                        <input type="number" class="form-control" required name="vin">
                                    </div><!-- /.model-select-icon -->
                                    @error('vin')
                                    <strong class="invalid-feedback d-block">{{ $message }}</strong>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-offset-1 col-md-2 col-sm-12">
                                <div class="single-model-search">
                                    <h2>{{ __('messages.model') }}</h2>
                                    <div class="model-select-icon">
                                        <select class="form-control" name="model_id" required disabled id="model">
                                        </select><!-- /.select-->
                                    </div><!-- /.model-select-icon -->
                                </div>
                                <div class="single-model-search">
                                    <h2>{{ __('messages.phone') }}</h2>
                                    <div class="model-select-without-icon">
                                        <input type="tel"
                                               id="phone"
                                               placeholder="+373 (79) 999 999"
                                               required class="form-control" name="phone">
                                    </div><!-- /.model-select-icon -->
                                </div>
                            </div>
                            <div class="col-md-offset-1 col-md-2 col-sm-12">
                                <div class="single-model-search">
                                    <h2>{{ __('messages.year') }}</h2>
                                    <div class="model-select-icon">
                                        <select class="form-control" name="year" required disabled id="year">
                                        </select><!-- /.select-->
                                    </div><!-- /.model-select-icon -->
                                </div>
                                <div class="single-model-search">
                                    <h2>{{ __('messages.email') }}</h2>
                                    <div class="model-select-without-icon">
                                        <input type="email" class="form-control" required name="email">
                                    </div><!-- /.model-select-icon -->
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-12" style="top: -43px;">
                                <div class="single-model-search text-center">
                                    <button type="submit" class="welcome-btn model-search-btn">
                                        {{ __('messages.place-order') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row">
                            <div class="col-md-offset-1" style="margin-right: 80px;">
                                <div class="single-model-search">
                                    <h2>{{ __('messages.description') }}</h2>
                                    <div class="model-select-without-icon">
                                    <textarea style="height: 150px;" class="form-control"
                                              name="description"></textarea>
                                    </div><!-- /.model-select-icon -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

</section>
<!--featured-cars start -->
<section id="featured-cars" class="featured-cars">
    <div class="container">
    </div><!--/.container-->

</section><!--/.featured-cars-->
<!--featured-cars end -->

<!--blog start -->
<section id="blog" class="blog"></section><!--/.blog-->
<!--blog end -->

<!--contact start-->
<footer id="contact" class="contact">
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="col-lg-12 col-lg-6">
                    <div class="single-footer-widget">
                        <div class="footer-logo">
                            <a href="/">{{ __('messages.welcome_title') }}</a>
                        </div>
                        <p>
ТУТ БУДЕТ ТЕКСТ
                        </p>
                        <br><br>
                        <div class="footer-contact">
                        <p>{{ __('messages.working-hours') }}</p>
                            <br>
                            <p>Пн-пт 09:00-18:00</p>
{{--                            <p>Вторник 09:00-18:00</p>--}}
{{--                            <p>Среда 09:00-18:00</p>--}}
{{--                            <p>Четверг 09:00-18:00</p>--}}
{{--                            <p>Пятница 09:00-18:00</p>--}}
                            <p>Суббота 09:00-14:00</p>

                        </div>
                        <br><br>
                        <div class="footer-contact">
                            <p>american.motorparts.srl@gmail.com</p>
                            <p>+373 (78) 904395</p>
                            <p>+373 (60) 693136</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-3 col-sm-6">
                    <div class="single-footer-widget">
                    </div></div>
                <div class="col-md-offset-1 col-md-3 col-sm-6">
                    <div class="single-footer-widget">
                        <h2>news letter</h2>
                        <div class="footer-newsletter">
                            <p>
                                {{ __('messages.subscribe') }}
                            </p>
                        </div>
                        <div class="hm-foot-email">
                            <div class="foot-email-box">
                                <input type="text" class="form-control" placeholder="Add Email">
                            </div><!--/.foot-email-box-->
                            <div class="foot-email-subscribe">
                                <span><i class="fa fa-arrow-right"></i></span>
                            </div><!--/.foot-email-icon-->
                        </div><!--/.hm-foot-email-->
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="row">
                <div class="col-sm-6">
                    <p>
                        {{__('Наши соцсети')}}
                    </p><!--/p-->
                </div>
                <div class="col-sm-6">
                    <div class="footer-social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-instagram"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-pinterest-p"></i></a>
                        <a href="#"><i class="fa fa-behance"></i></a>
                    </div>
                </div>
            </div>
        </div><!--/.footer-copyright-->
    </div><!--/.container-->

    <div id="scroll-Top">
        <div class="return-to-top">
            <i class="fa fa-angle-up " id="scroll-top" data-toggle="tooltip" data-placement="top" title=""
               data-original-title="Back to Top" aria-hidden="true"></i>
        </div>

    </div><!--/.scroll-Top-->

</footer><!--/.contact-->
<!--contact end-->


<!-- Include all js compiled plugins (below), or include individual files as needed -->

<script src="js/jquery.js"></script>

<!--modernizr.min.js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>

<!--bootstrap.min.js-->
<script src="js/bootstrap.min.js"></script>

<!-- bootsnav js -->
<script src="js/bootsnav.js"></script>

<!--owl.carousel.js-->
<script src="js/owl.carousel.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>

<!--Custom JS-->
<script src="js/custom.js"></script>
<script type="text/javascript">
    $("#contact-us").on('click', function () {
        $('html, body').scrollTop($("#contact-form").offset().top);
    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#make').change(function () {
        event.preventDefault();
        $('#model').empty();
        $.ajax({
            url: "get-makes",
            type: "POST",
            data: {"vehicle_id": $("#make").val()},
            success: function (states) {
                $('#model').removeAttr('disabled').attr("required", "true");
                $('#model').append('<option selected disabled hidden></option>');
                $.each(states, function (i, model) {
                    $('#model').append($('<option>', {
                        value: model.id,
                        text: model.name
                    }));
                });
            }
        });
    });
    $('#model').change(function () {
        event.preventDefault();
        $('#year').empty();
        $.ajax({
            url: "get-model-years",
            type: "POST",
            data: {"model_id": $("#model").val()},
            success: function (states) {
                $('#year').removeAttr('disabled').attr("required", "true");
                $('#year').append('<option selected disabled hidden></option>');
                $.each(states, function (i, model) {
                    $('#year').append($('<option>', {
                        value: model.year,
                        text: model.year
                    }));
                });
            }
        });
    });

    // $('#language_ru').on('click', function () {
    //     console.log(this.href);
    //     window.location.replace(this.href);
    // });
    // $('#language_ro').on('click', function () {
    //     console.log(this.href);
    //     window.location.replace(this.href);
    // });
    // $('#language_en').on('click', function () {
    //     console.log(this.href);
    //     window.location.replace(this.href);
    // });
    {{--$('#language_ro').prop('href', {{ url/set-locale/ro') }});--}}
    {{--$('#language_en').prop('href', {{ url/set-locale/en') }});--}}

    $("#phone").mask("+373 (99) 999 999");
</script>

</body>

</html>
