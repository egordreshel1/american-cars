<?php

namespace App\Filament\Resources;

use App\Filament\Resources\ImageResource\Pages;
use App\Filament\Resources\ImageResource\RelationManagers;
use App\Models\Image;
use Filament\Forms;
use Filament\Forms\Components\FileUpload;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\ImageColumn;
use Filament\Tables\Filters\QueryBuilder\Constraints\NumberConstraint;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class ImageResource extends Resource
{
    protected static ?string $model = Image::class;

    protected static ?string $navigationIcon = 'heroicon-m-photo';

    protected static ?int $navigationSort = 4;

    public static function form(Form $form): Form
    {
        $order = range(1, 1000);

        $query = Image::query();

        if (is_a($form->model, Image::class)) {
            $query->whereNot('id', $form->model->getKey());
        }

        $takenOrders = $query
            ->get()
            ->pluck('order')
            ->toArray();

        $availableNumbers = [];

        foreach (array_diff($order, $takenOrders) as $value) {
            $availableNumbers[$value] = $value;
        }


        return $form
            ->schema([
                FileUpload::make('filename')
                    ->label(__('messages.image'))
                    ->disk('public')
                    ->maxSize(102400)
                    ->image()
                    ->imageEditor()
                    ->openable(),
                Forms\Components\Select::make('order')
                    ->label(__('messages.image-order'))
                    ->options(
                        $availableNumbers,
                    ),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                ImageColumn::make('filename')->label(__('messages.image')),
                Tables\Columns\TextColumn::make('order')->label(__('messages.image-order')),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListImages::route('/'),
            'create' => Pages\CreateImage::route('/create'),
            'edit' => Pages\EditImage::route('/{record}/edit'),
        ];
    }

    public static function getNavigationLabel(): string
    {
        return __('messages.images');
    }
}
