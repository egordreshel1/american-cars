<?php

namespace App\Filament\Resources;

use App\Filament\Resources\OrderResource\Pages;
use App\Filament\Resources\OrderResource\RelationManagers;
use App\Models\Order;
use Filament\Forms;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Table;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class OrderResource extends Resource
{
    protected static ?string $model = Order::class;

    protected static ?string $navigationIcon = 'heroicon-m-currency-dollar';

    protected static ?int $navigationSort = 3;

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                Forms\Components\Select::make('vehicle_id')
                    ->label(__('messages.vehicle'))
                    ->searchable()
                    ->preload()
                    ->relationship('vehicle', 'name')
                    ->required(),
                Forms\Components\Select::make('model_id')
                    ->label(__('messages.model'))
                    ->searchable()
                    ->preload()
                    ->relationship('model', 'name')
                    ->required(),
                Forms\Components\TextInput::make('email')
                    ->label(__('messages.email'))
                    ->label(__('messages.email'))
                    ->email()
                    ->required()
                    ->maxLength(255),
                Forms\Components\TextInput::make('phone')
                    ->label(__('messages.phone'))
                    ->label('Phone number')
                    ->required(),
                Forms\Components\TextInput::make('year')
                    ->label(__('messages.year'))
                    ->label('Year')
                    ->required(),
                Forms\Components\TextInput::make('vin')
                    ->label('VIN')
                    ->label('VIN')
                    ->required(),
                Forms\Components\Textarea::make('description')
                    ->label(__('messages.description'))
                    ->maxLength(65535)
                    ->columnSpan('full'),
                Forms\Components\Textarea::make('notes')
                    ->label(__('messages.notes'))
                    ->maxLength(65535)
                    ->columnSpan('full'),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                Tables\Columns\TextColumn::make('email')
                    ->label(__('messages.email'))
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('phone')
                    ->label(__('messages.phone'))
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('vin')
                    ->label('VIN')
                    ->searchable()
                    ->sortable(),
                Tables\Columns\TextColumn::make('vehicle.name')
                    ->label(__('messages.vehicle'))
                    ->searchable(),
                Tables\Columns\TextColumn::make('model.name')
                    ->label(__('messages.model'))
                    ->searchable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListOrders::route('/'),
            'create' => Pages\CreateOrder::route('/create'),
            'edit' => Pages\EditOrder::route('/{record}/edit'),
        ];
    }

    public static function getNavigationLabel(): string
    {
        return __('messages.orders');
    }
}
