<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property string $vin
 * @property string $phone
 * @property string $email
 * @property string $description
 * @property string $year
 * @property string $notes
 * @property integer $model_id
 * @property integer $make_id
 * @property Model $model
 */
class Order extends EloquentModel
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'vin',
        'phone',
        'email',
        'description',
        'model_id',
        'year',
        'vehicle_id',
        'notes',
    ];

    public function model(): BelongsTo
    {
        return $this->belongsTo(Model::class);
    }

    public function vehicle(): BelongsTo
    {
        return $this->belongsTo(Vehicle::class);
    }
}
