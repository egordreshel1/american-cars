<?php

declare(strict_types=1);

namespace App\Models\Dtos;


use App\Models\Order;
use Spatie\LaravelData\Data;

class OrderDto extends Data
{
    public ?int $id = null;

    public int $model_id;

    public int $vehicle_id;

    public ?string $notes = null;

    public ?string $description = null;

    public string $phone;

    public string $email;

    public string $year;

    public string $vin;

    #[Hidden]
    public string $dataClass = Order::class;
}
