<?php

declare(strict_types=1);

namespace App\Actions;

use App\Models\Model;
use App\Models\Vehicle;
use Carbon\Carbon;
use Illuminate\Support\Collection;

final class GetModelYearsByMakeAction
{
    public function handle(array $data): Collection
    {
        $modelId = $data['model_id'];

        if ($modelId !== null && $modelId !== __('messages.other')) {
            $model = Model::query()
                ->findOrFail($modelId);

            $result = [];
            foreach (range($model->year_start, $model->year_end ?? Carbon::now()->year) as $year) {
               $result[] = ['year' => $year];
            }

            return collect($result);
        }

        return collect([['year' => __('messages.other')]]);
    }
}
