<?php

declare(strict_types=1);

namespace App\Actions;

use App\Models\Dtos\OrderDto;
use App\Models\Order;
use Illuminate\Support\Facades\DB;

final class StoreOrderAction
{
    public function handle(array $data): Order
    {
        try {
            DB::beginTransaction();
            $order = Order::create(OrderDto::from($data)->toArray());

            DB::commit();

            return $order;
        } catch (\Exception $exception) {
            DB::rollBack();

            throw $exception;
        }
    }
}
