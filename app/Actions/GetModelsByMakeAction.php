<?php

declare(strict_types=1);

namespace App\Actions;

use App\Models\Model;
use App\Models\Vehicle;
use Illuminate\Support\Collection;

final class GetModelsByMakeAction
{
    public function handle(array $data): Collection
    {
        $makeId = $data['vehicle_id'];

        if ($makeId !== null) {
            return Vehicle::query()
                ->with('models')
                ->findOrFail($makeId)
                ->models
                ->map(fn(Model $model) => ['id' => $model->getKey(), 'name' => $model->name]);
        }

        return collect([['id' => null, 'name' => __('messages.other')]]);
    }
}
