<?php

declare(strict_types=1);

namespace App\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModelListRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'vehicle_id' => 'numeric|nullable|exists:vehicles,id',
        ];
    }
}
