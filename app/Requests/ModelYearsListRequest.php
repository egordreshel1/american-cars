<?php

declare(strict_types=1);

namespace App\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModelYearsListRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'model_id' => 'required',
        ];
    }
}
