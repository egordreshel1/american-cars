<?php

declare(strict_types=1);

namespace App\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrderRequest extends FormRequest
{
    public function rules(): array
    {
        return [
            'vehicle_id' => 'required',
            'model_id' => 'required',
            'year' => 'required|string',
            'vin' => 'required|string',
            'phone' => 'required|string',
            'email' => 'required|email',
            'description' => 'required|string'
        ];
    }
}
