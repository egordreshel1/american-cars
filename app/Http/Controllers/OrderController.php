<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Actions\GetModelsByMakeAction;
use App\Actions\GetModelYearsByMakeAction;
use App\Actions\StoreOrderAction;
use App\Requests\ModelListRequest;
use App\Requests\ModelYearsListRequest;
use App\Requests\StoreOrderRequest;
use Illuminate\Http\JsonResponse;

class OrderController extends Controller
{
    public function getModels(ModelListRequest $request, GetModelsByMakeAction $action): JsonResponse
    {
        return response()->json($action->handle($request->validated()));
    }

    public function getModelYears(ModelYearsListRequest $request, GetModelYearsByMakeAction $action): JsonResponse
    {
        return response()->json($action->handle($request->validated()));
    }

    public function placeOrder(StoreOrderRequest $request, StoreOrderAction $action)
    {
        $action->handle($request->validated());

        return redirect('/')->with('success', __('order.placed'));
    }
}
