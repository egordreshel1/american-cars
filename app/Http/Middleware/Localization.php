<?php

namespace App\Http\Middleware;

use Carbon\Carbon;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class Localization
{
    public function handle(Request $request, Closure $next)
    {
        if (session()->has('locale')) {
            Carbon::setLocale(session()->get('locale'));
            App::setLocale(session()->get('locale'));
        }

        return $next($request);
    }
}
