<?php

return [
    'welcome_title' => 'AMERICAN MOTORPARTS',
    'other' => 'Other',
    'image' => 'Image',
    'image-order' => 'Image order',
    'password' => 'Password',
    'email' => 'Email',
    'name' => 'Name',
    'vehicle' => 'Make',
    'year-start' => 'Year start',
    'year-end' => 'Year end',
    'model' => 'Model',
    'phone' => 'Phone',
    'year' => 'Year',
    'description' => 'Description',
    'notes' => 'Notes',

    'home' => 'Home',
    'contact' => 'Contact',
    'place-order' => 'Place order',

    'images' => 'Images',
    'vehicles' => 'Makes',
    'models' => 'Models',
    'users' => 'Users',
    'orders' => 'Orders',

    'subscribe' => 'Subscribe to get latest news update and informations',
    'welcome-title' => 'The best for your car',
    'contact-us' => 'contact us',
    'parts-information' => 'Arrival information',
    'working-hours' =>'Working hours',
];
