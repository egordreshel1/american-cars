<?php

return [
    'other' => 'Altele',
    'image' => 'Imagine',
    'image-order' => 'Comanda de imagini',
    'password' => 'Parola',
    'email' => 'Email',
    'name' => 'Nume',
    'vehicle' => 'Marca auto',
    'year-start' => 'Începutul anului',
    'year-end' => 'Sfârșitul anului',
    'model' => 'Model',
    'phone' => 'Telefon',
    'year' => 'Anul',
    'description' => 'Descriere',
    'notes' => 'Note',

    'home' => 'Acasă',
    'contact' => 'Contactați',
    'place-order' => 'Plasați comanda',

    'images' => 'Imagini',
    'vehicles' => 'Marci auto',
    'models' => 'Modele',
    'users' => 'Utilizatori',
    'orders' => 'Comenzi',

    'subscribe' => 'Subscribe to get latest news update and informations',
    'contact-us' => 'Plasați o comandă',
    'parts-information' => 'Informații de sosire',

];
