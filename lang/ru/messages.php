<?php

return [
    'other' => 'Другие',
    'image' => 'Изображение',
    'image-order' => 'Порядок изображения',
    'password' => 'Пароль',
    'email' => 'Email',
    'name' => 'Имя',
    'vehicle' => 'Марка',
    'year-start' => 'Начало производства',
    'year-end' => 'Конец производства',
    'model' => 'Модель',
    'phone' => 'Телефон',
    'year' => 'Год',
    'description' => 'Описание',
    'notes' => 'Заметки',

    'home' => 'Главная',
    'contact' => 'Контакты',
    'place-order' => 'Разместить заказ',

    'images' => 'Изображения',
    'vehicles' => 'Марки',
    'models' => 'Модели',
    'users' => 'Пользователи',
    'orders' => 'Заказы',

    'subscribe' => 'Subscribe to get latest news update and informations',
    'contact-us' => 'Разместить заказ',
    'parts-information' => 'Информация о поступлении товара',
    'working-hours' =>'График работы',
];
